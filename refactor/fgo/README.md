# Soal Latihan Refactoring 1

Pada soal latihan ini, Anda diharapkan dapat melakukan *refactoring* tanpa mengubah fungsionalitas dari program. Silahkan identifikasi *code smell* dan lakukan *refactoring*.

## Cara menjalankan program

1. Jalankan perintah `javac Refactor1Main.java`
2. Jalankan perintah `java Refactor1Main`
3. Output akan langsung dicetak oleh program
4. Silahkan modifikasi kelas `Refactor1Main` jika diperlukan