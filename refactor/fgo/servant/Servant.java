package servant;

public abstract class Servant {
	protected String name;
	protected String type;

	public Servant(String name){
		this.name = name;
	}

	public String attackPattern(){
		StringBuilder sb = new StringBuilder();
		sb.append(attackWithBuster()).append("\n");
		sb.append(attackWithQuick()).append("\n");
		sb.append(attackWithArts()).append("\n");
		sb.append("==============================");
		return sb.toString();
	}

	@Override
	public String toString(){
		return String.format("servant name : %s, servant class : %s", name, getType());
	}

	public String getType(){
		return type;
	}
	
	public abstract String attackWithBuster();
	public abstract String attackWithQuick();
	public abstract String attackWithArts();
}