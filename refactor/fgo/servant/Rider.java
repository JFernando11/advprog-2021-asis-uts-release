package servant;

public class Rider extends Servant {
	public Rider(String name){
		super(name);
		this.type = "Rider";
	}

	@Override
	public String getType(){
		return "Rider";
	}

	@Override
	public String attackWithBuster(){
		return String.format("%s attacks with buster. Effects : regular", name);
	}

	@Override
	public String attackWithQuick(){
		return String.format("%s attacks with quick. Effects : ineffective", name);
	}

	@Override
	public String attackWithArts(){
		return String.format("%s attacks with arts. Effects : super effective", name);
	}
}