package servant;

public class Berserker extends Servant {
	public Berserker(String name){
		super(name);
		this.type = "Berserker";
	}

	@Override
	public String getType(){
		return "Berserker";
	}

	@Override
	public String attackWithBuster(){
		return String.format("%s attacks with buster. Effects : super effective", name);
	}

	@Override
	public String attackWithQuick(){
		return String.format("%s attacks with quick. Effects : regular", name);
	}

	@Override
	public String attackWithArts(){
		return String.format("%s attacks with arts. Effects : ineffective", name);
	}
}