# Soal Refactoring
Pada topik *refactoring*, soal yang disediakan akan berjumlah lebih dari satu dan mungkin akan ditambah. Silahkan periksa *folder*/*package* dari masing-masing soal dan baca *file* `README.md` untuk ketentuan di masing-masing soal

Soal yang tersedia saat ini:
1. *problem set* pada *package* `asdos`
2. *problem set* pada *package* `fgo`