public class Refactor2Main {
	public static void main(String[] args){
		AsdosLog bambang = new AsdosLog("Bambang", "1001221241", 3, "Mahasiswa S1");
		AsdosOutsource budi = new AsdosOutsource("Budi", "OUT-ASEQD-12112", 1, "Lulus S2");
		AsdosOutsource rizki = new AsdosOutsource("Rizki", "OUT-AIAWD-12212", 5, "Lulus S3");
		AsdosLog asdosGila = new AsdosLog("ASDOS GILA BUDAK UANG", "2105599114", 10, "Lulus S1");

		bambang.tambahLog("Basis Data", "BASDAT", 2.3, "Sesi Tutorial PostgreSQL", "Lab Fasiklom UIIII");
		budi.tambahLog("Sains Data", "DSA", 3.8, "Membuat PR 1", "Kediaman Asdos");
		rizki.tambahLog("Penjaminan Mutu Perangkat Lunak", "PMPL", 8, "Set up SonarQube", "Lab Unreliable Software Engineering");
		asdosGila.tambahLog("Pemrograman Lanjut", "ADVPROG", 40, "Membuat soal latihan UTS super susah", "Server Discord");

		System.out.println(bambang);
		System.out.println(budi);
		System.out.println(rizki);
		System.out.println(asdosGila);
	}
}