package collection;

import collection.iterator.Iterator;
import collection.iterator.DecoyIterator;

public class ChainedQueue<T> implements Collection<T> {
	private static final class Node<T>{
		T elem;
		Node<T> next;

		Node(T elem, Node<T> next){
			this.elem = elem;
			this.next = next;
		}

		Node(T elem){
			this(elem, null);
		}

		Node(){
			this(null, null);
		}

		T getElem(){
			return elem;
		}
	}

	private Node<T> head;
	private Node<T> current;

	public ChainedQueue(){
		this.head = new Node<>();
		current = head;
	}

	@Override
	public void add(T elem){
		current = current.next = new Node<>(elem);
	}

	@Override
	public boolean isEmpty(){
		return current == head;
	}

	@Override
	// Notes : anda mungkin ingin mendefinisikan kelas iterator di dalam kelas ini (ChainedQueue)
	public Iterator<T> getIterator(){
		return new DecoyIterator<>();
	}
}