package collection;

import collection.iterator.Iterator;

public interface Collection<T> {
	public void add(T elem);
	public boolean isEmpty();
	public Iterator<T> getIterator();
}