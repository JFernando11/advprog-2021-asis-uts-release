package collection;

import java.util.Arrays;

public abstract class CustomArray<T> implements Collection<T> {
	protected T[] array;
	protected int size;

	private static final int DEFAULT_CAPACITY = 10;

	public CustomArray(int initCapacity){
		this.array = (T[]) new Object[initCapacity];
		size = 0;
	}

	public CustomArray(){
		this(DEFAULT_CAPACITY);
	}

	@Override
	public void add(T elem){
		if(size >= array.length){
			T[] newArray = (T[]) new Object[size * 5];
			for(int i = 0; i < size; i++){
				newArray[i] = array[i];
 			}
 			array = newArray;
		}
		array[size] = elem;
		size++;
	}

	@Override
	public boolean isEmpty(){
		return size == 0;
	}
}