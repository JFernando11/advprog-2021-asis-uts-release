package guild;

import adventurer.Adventurer;

import comparator.ComparableComparator;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Guild {
	private List<Adventurer> advList;
	private Comparator<Adventurer> currentComparator;

	public Guild(){
		advList = new ArrayList<>();
		currentComparator = new ComparableComparator();
	}

	public void addAdventurer(Adventurer adv){
		advList.add(adv);
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		for(Adventurer adventurer : advList){
			sb.append(adventurer.toString()).append("\n");
		}
		sb.append("=====================\n");

		return sb.toString();
	}

	public void sortById(){
		// TO DO : Lengkapi bagian ini
		// Hint : Gunakan Collections.sort()
		// Hint : Manfaatkan atribut currentComparator
	}

	public void sortByName(){
		// TO DO : Lengkapi bagian ini
		// Hint : Gunakan Collections.sort()
		// Hint : Manfaatkan atribut currentComparator
	}

	public void sortByHp(){
		// TO DO : Lengkapi bagian ini
		// Hint : Gunakan Collections.sort()
		// Hint : Manfaatkan atribut currentComparator
	}

	public void sortByAttack(){
		// TO DO : Lengkapi bagian ini
		// Hint : Gunakan Collections.sort()
		// Hint : Manfaatkan atribut currentComparator
	}

	public void sortBySpeed(){
		// TO DO : Lengkapi bagian ini
		// Hint : Gunakan Collections.sort()
		// Hint : Manfaatkan atribut currentComparator
	}
}