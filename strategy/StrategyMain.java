import adventurer.Adventurer;
import guild.Guild;

public class StrategyMain {
	public static void main(String[] args){
		Guild guild = new Guild();
		Adventurer[] advArr = {
			new Adventurer("Bryan", 100,75,133),
			new Adventurer("Artur", 124,87,96),
			new Adventurer("Daniele", 77, 123, 111),
			new Adventurer("Colt", 87, 100, 100)
		};

		for(Adventurer adv : advArr){
			guild.addAdventurer(adv);
		}

		System.out.println(guild);
		guild.sortById();
		System.out.println(guild);
		guild.sortByName();
		System.out.println(guild);
		guild.sortByHp();
		System.out.println(guild);
		guild.sortByAttack();
		System.out.println(guild);
		guild.sortBySpeed();
		System.out.println(guild);
	}
}