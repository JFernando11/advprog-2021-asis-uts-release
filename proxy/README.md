# Soal Latihan Proxy Pattern

Pada soal latihan ini, Anda diminta untuk mengimplementasikan Proxy Pattern dengan benar.

## Requirements

Ceritanya, ada sebuah aplikasi *name counter* yang nantinya akan digunakan di banyak tempat sekaligus.
Kalian diminta untuk membuat simulasi (err... *might be oversimplified one*) dari *Load Balancer* untuk beberapa instance `NameCounter`.

Berikut ini adalah penjelasan dari setiap package yang disediakan:
- **Package `repository`** berisi kelas Singleton `NameRepository` yang nantinya akan menyimpan daftar nama dari semua instance.
- **Package `core`** berisi berkas-berkas utama dari program ini.
  Kami hanya menyediakan kelas `NameCounterInstance`, sesuai namanya, merupakan *instance* dari aplikasi *name counter*.

Aplikasi ini nantinya bisa melakukan 4 hal:
- Registrasi nama.
- Mendapatkan nama berdasarkan *entry number*.
- Menghitung berapa kali sebuah nama diregistrasi.
- Orang yang pertama kali mendaftar bisa mendapatkan daftar semua nama.

Tentunya, *Load Balancer* yang kamu buat nantinya akan mencarikan *instance* yang sedang lowong.
Hint: definisi "lowong" ada di tangan kalian (mau itu *randomized*, mau itu pakai `Queue` atau mungkin `PriorityQueue`, dibebaskan).
*Instance* dari `NameCounter` tentunya tidak boleh diakses secara langsung, harus melalui *load balancer* yang kamu buat.

Oh iya, dalam proses instansiasi *load balancer* tersebut, kamu perlu memasukkan jumlah dari *instance* yang ingin kamu buat.
Karena ini simulasi, semua *instance* dibuat pada saat pertama kali *running*. Nantinya, mungkin akan diubah ketika sudah siap rilis.

Telah disediakan juga berkas `ProxyMain` yang bisa mensimulasikan jalannnya program.

## Cara menjalankan program

1. Jalankan perintah `javac ProxyMain.java`
2. Jalankan perintah `java ProxyMain`
3. Output akan langsung dicetak oleh program
