package stance.misc;

import stance.*;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.Arrays;

public class StanceTransitionHandler {
	private static StanceTransitionHandler instance = new StanceTransitionHandler();
	private static Map<String, List<Stance>> typeMapper;

	private StanceTransitionHandler(){
		typeMapper = new TreeMap<>();
		typeMapper.put("En garde", Arrays.asList(StanceRepository.ENGARDE, StanceRepository.RETREAT, StanceRepository.PARRY));
		typeMapper.put("Parry", Arrays.asList(StanceRepository.PARRY, StanceRepository.ENGARDE));
		typeMapper.put("Retreat", Arrays.asList(StanceRepository.RETREAT, StanceRepository.ENGARDE));
		typeMapper.put("Unstable", Arrays.asList(StanceRepository.UNSTABLE));
		typeMapper.put("Lost", Arrays.asList(StanceRepository.LOST));
	}

	public List<Stance> getAvailableStances(String type){
		return typeMapper.get(type);
	}

	public static StanceTransitionHandler getInstance(){
		return instance;
	}
}