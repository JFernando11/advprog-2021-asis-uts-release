package stance;

import java.util.Arrays;
import java.util.List;
import attack.Attack;
import stance.misc.StancePair;
import stance.misc.StanceRepository;

public class Retreat extends Stance {
	public Retreat(){
		super("Retreat", "A defensive mechanism done by doing a step backward", 
			Arrays.asList(Attack.SECOND_INTENTION, Attack.LUNGE)
		);
	}
}