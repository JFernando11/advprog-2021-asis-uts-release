package magic;

import adventurer.Adventurer;

public class AreaMagic {
	private String name;
	private String type;
	private int power;

	public AreaMagic(String name, String type, int power){
		this.name = name;
		this.type = type;
		this.power = power;
	}

	public String getName(){
		return name;
	}

	public String getType(){
		return type;
	}

	public int getPower(){
		return power;
	}

	public void addAdventurer(Adventurer adventurer){
		// TO DO : Silahkan lengkapi method ini
		// Hint	: 
		//  Anda mungkin ingin menambahkan atribut pada kelas ini 
		//  dan mengubah implementasi dari metode tertentu
	}

	public String cast(){
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("Magic %s (type: %s) with power %d is cast", name, type, power));
		sb.append("\n------------------");

		return sb.toString();
	}
}