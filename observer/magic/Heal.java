package magic;

public class Heal extends AreaMagic {
	public Heal(String name, int power){
		super(name, "heal", power);
	}
}