# Soal Latihan Observer Pattern

Pada soal latihan ini, Anda diminta untuk mengimplementasikan Observer Pattern dengan benar.

## Requirements
Terdapat 4 jenis `AreaMagic` dan 4 jenis `Adventurer`. 4 jenis `AreaMagic` beserta efeknya adalah sebagai berikut:
1. `Curse`, mengurangi `hp` sebanyak `power` namun meningkatkan `attack` dan `speed` sebanyak `power`
2. `Heal`, meningkatkan `hp` sebanyak `power` namun mengurangi `attack` dan `speed` sebanyak `power`
3. `Haste`, meningkatkan `speed` sebanyak `power`
4. `Strengthen`, meningkatkan `attack` sebanyak `power`

(Notes : `power` merupakan atribut dari `AreaMagic` sedangkan `hp`, `attack`, dan `speed` merupakan atribut dari `Adventurer`)

Setiap jenis `Adventurer` akan memiliki reaksi yang berbeda saat menerima efek dari `AreaMagic` tertentu, perbedaan reaksi yang terjadi terhadap masing-masing `Adventurer` adalah sebagai berikut:
1. `DeathKnight` tidak akan menerima pengurangan `hp` saat `Curse` dipanggil dan tidak akan menerima peningkatan `hp` saat `Heal` dipanggil
2. `Paladin` tidak akan menerima peningkatan `attack` dan `speed` saat `Curse` dipanggil dan tidak akan menerima pengurangan `attack` dan `speed` saat `Heal` dipanggil
3. `Duellist` akan menerima peningkatan `speed` sebanyak dua kali nilai `power` saat `Haste` dipanggil
4. `Warrior` akan menerima peningkatan `attack` sebanyak dua kali nilai `power` saat `Strengthen` dipanggil

## Cara menjalankan program

1. Jalankan perintah `javac ObserverMain.java`
2. Jalankan perintah `java ObserverMain`
3. Output akan langsung dicetak oleh program