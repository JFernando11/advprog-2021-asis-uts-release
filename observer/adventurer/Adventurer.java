package adventurer;

import magic.AreaMagic;

// TO DO: 
//	Silahkan tambahkan atribut atau metode yang mungkin Anda perlukan
//	supaya Observer Pattern dapat diimplementasi dengan benar
public abstract class Adventurer {
	private String name;
	private String advType;
	protected int hp;
	protected int attack;
	protected int speed;

	public Adventurer(String name, int hp, int attack, int speed, String advType){
		this.name = name;
		this.hp = hp;
		this.attack = attack;
		this.speed = speed;
		this.advType = advType;
	}

	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(name + "\n");
		sb.append("class : " + advType + "\n");
		sb.append("HP : " + hp + "\n");
		sb.append("ATK : " + attack + "\n");
		sb.append("SPD : " + speed + "\n");
		sb.append("================");
		return sb.toString();
	}
}