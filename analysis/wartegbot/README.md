# WartegBot

Bung Komar, seorang wirausaha kuliner ingin mengembangkan usaha warteg-nya sehingga bisa melayani mahasiswa penghuni kos di seluruh kota Depok.
Setelah melakukan *research* singkat, Bung Komar menemukan bahwa mahasiswa lebih sering menggunakan Discord sebagai layanan *chatting* utama mereka.
Sehingga, Bung Komar terpikir untuk membuat sebuah Discord *chat bot* sehingga mahasiswa bisa **memesan makanan melalui *chat bot* tersebut**.

Namun, tidak seperti *chat bot* pada umumnya, Bung Komar ingin membuat *chat bot*-nya memiliki <u>alur percakapan yang natural</u>.
Bung Komar pun mulai membuat skenario bagaimana percakapan tersebut nantinya dilakukan:

![Flow interaksi WartegBot](images/wartegbot_flow.png)
Caption: Flow interaksi WartegBot.

## Bung Komar butuh bantuan kalian, yaitu...

-  [ ] **Tentukan *design pattern* apa yang bisa digunakan oleh Bung Komar.**<br>
   **HINT:** Kalian boleh menggabungkan lebih dari satu *design pattern* (tentunya kalian sudah belajar kan terkait itu :D)
-  [ ] **Tuliskan catatan terkait rancangan *class* atau tahapan implementasinya di `notes.md`.**<br>
   **HINT:** Tuliskan juga asumsi yang ingin kalian gunakan dalam merancang hal tersebut.
-  [ ] **Simulasikan hal tersebut dengan menggunakan *vanilla* Java!**<br>
   Tentunya, kalian tidak perlu sampai memikirkan *framework*, karena Bung Komar saat ini hanya perlu ide dari kalian.
